
## Měření
Měření proběhlo vůči kódu v commitu 3b11aea88bc, na 4 jádrovém i5-6600K


# π-estimator

π-estimator je program pro aproximaci π za pomocí Monte Carlo simulace.
V jedné iteraci je vygenerován náhodný bod z [0, 1]x[0, 1] a zjistí se,
jestli vygenerovaný bod spadá do jednotkového kruhu. Po vygenerování N
bodů se π dá spočítat jako 4 * <množství bodů uvnitř kruhu> / N.

## Implementace
Implementace pro jedno vlákno je triviální, vícevláknová implementace
pak vydělí množství iterací množstvím spuštěných vláken a nechá každé
vlákno provést implementaci pro jedno vlákno. Množství vláken je určeno
dle množství jader počítače, jejich synchronizace je triviální, přes
atomickou proměnnou.

Množství iterací je předáno jako druhý argument. Pokud druhý argument
není předán, pak je použita defaultní hodnota 10M iterací.

## Měření
Měření proběhlo vůči kódu v commitu 3b11aea88bc, na 4 jádrovém i5-6600K
CPU taktovaném na 3.5 GHz a s vypnutým HT. Vyšlo mi, že jednovláknová
varianta potřebuje 458ms a vícevláknová varianta se 4 vlákny potřebuje
116ms pro provedení 10M iterací.

Výstup z měření
```
ST inside points: 7853087
MT inside points: 7853934
PI value: 3.14159
ST PI time needed: 458ms value: 3.14123
MT PI time needed: 116ms value: 3.14157
```
